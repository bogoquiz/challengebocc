import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as Product from './data.json';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  constructor() { }

  public getProduct(): Observable<any> {
    return of(Product) 
  }

}

