import { Component, OnInit } from '@angular/core';
import { SharedDataService } from '../model/shared-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  banks:any = [];
  products:object;
  bankSelected:string = 'BANCO_1';

  constructor(
    public sharedService: SharedDataService
  ) {
    console.log(this.banks)
   }

  ngOnInit() {
    this.get_products()
    this.getBanks()
    this.getProductByBank()
  }


  get_products(){
    this.sharedService.getProduct().subscribe(res=>{
        console.log(res["default"]);
    })        
  }

  changeBank(value) {
    this.bankSelected =value;
    this.getProductByBank()
  }

  getProductByBank(){
    this.sharedService.getProduct().subscribe(res=>{
      this.products = res["default"].product.filter(prod=>{
        return prod["accountInformation"]["bank"] === this.bankSelected;
      })
    })
  }

  getBanks(){
    this.sharedService.getProduct().subscribe(res=>{
      let bankers
      bankers = res["default"].product.map(item => {
        return item["accountInformation"]["bank"]
       }
      )
      this.banks = [...new Set(bankers)].sort()
    })
  }
}
